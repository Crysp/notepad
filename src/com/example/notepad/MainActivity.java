package com.example.notepad;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private final static String SDCARD_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
	private StringBuilder content;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        final EditText fileField = (EditText) findViewById(R.id.fileField);
        final EditText searchField = (EditText) findViewById(R.id.searchField);
        final EditText contentField = (EditText) findViewById(R.id.contentField);
        
        final Button openButton = (Button) findViewById(R.id.openButton);
        final Button searchButton = (Button) findViewById(R.id.searchButton);
        
        fileField.append("lipsum.txt");
        
        openButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try {
					content = getContent(SDCARD_PATH + "/" + fileField.getText());
					contentField.append(content);
				} catch(IOException e) {
					Toast.makeText(MainActivity.this, "Can't open file", Toast.LENGTH_SHORT).show();
				}
			}
		});
        
        searchButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String str = searchField.getText().toString();
				if(!str.matches("")) {
					int startIndex = content.indexOf(str, 0);
					if(startIndex != -1) {
						contentField.setSelection(startIndex, startIndex + str.length());
					} else {
						Toast.makeText(MainActivity.this, "No matches", Toast.LENGTH_SHORT).show();
					}
				}
				
			}
		});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    private StringBuilder getContent(String filename) throws IOException {
    	StringBuilder content = new StringBuilder();
    	BufferedReader reader = new BufferedReader(new FileReader(new File(filename)));
    	try {
    		String line;
    		while((line = reader.readLine()) != null) {
    			content.append(line).append("\n");
    		}
    	} finally {
    		reader.close();
    	}
    	return content;
    }
    
}
